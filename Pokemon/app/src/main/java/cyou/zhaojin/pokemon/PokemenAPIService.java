package cyou.zhaojin.pokemon;
/* Description :
 *
 * Date 15/02/2021
 * Author Zhao JIN
 */

import com.google.gson.JsonElement;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface PokemenAPIService {
    @GET
    Call<JsonElement> listPokemons(@Url String url);

    @GET("pokemon/{IDPOKEMON}")
    Call<JsonElement> getPokemon(@Path("IDPOKEMON") String id);
}
