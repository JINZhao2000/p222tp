package cyou.zhaojin.pokemon;

import android.content.ComponentName;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity2 extends AppCompatActivity {
    private static String TAG = "MainActivity2:LOG";

    Retrofit retrofit;
    PokemenAPIService servicePokemon;
    private static final String API_BASE_URL = "https://pokeapi.co/api/v2/";
    private static final String API_IMAGE = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/";
    private volatile String name = "?";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        this.retrofit = new Retrofit.Builder()
                .baseUrl(MainActivity2.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        this.servicePokemon = retrofit.create(PokemenAPIService.class);

        int pid = getIntent().getIntExtra("pokemon", 0);

        Call<JsonElement> appel = servicePokemon.getPokemon(Integer.toString(pid));
        appel.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                if (response.isSuccessful()) {
                    JsonElement contenu = response.body();
                    JsonObject jsonGlobal = contenu.getAsJsonObject();
                    name = jsonGlobal.get("name").getAsString();
                    TextView textView = findViewById(R.id.textP);
                    textView.setText(name);
                    ImageView imageView = findViewById(R.id.imageP);
                    imageView.setImageBitmap(new DownloadImageTask(imageView).doInBackground(API_IMAGE+pid+".png"));
                    MainActivity2.this.setTitle(name);
                } else {
                    Toast.makeText(MainActivity2.this, "Erreur lors de l'appel à l'API :" + response.errorBody(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                Toast.makeText(MainActivity2.this, "Erreur lors de l'appel à l'API :" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        Button button = (Button) findViewById(R.id.retour);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}