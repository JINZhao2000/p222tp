package cyou.zhaojin.pokemon;
/* Description :
 *
 * Date 10/02/2021
 * Author Zhao JIN
 */

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

public class ListItem implements Comparable<ListItem>{
    private Drawable image;
    private String title;
    private int pid;

    public ListItem() {
    }

    public ListItem(String title, Drawable drawable){
        this.image = drawable;
        this.title = title;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public Drawable getImage() {
        return image;
    }

    public void setImage(Drawable image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public int compareTo(ListItem o) {
        return this.getTitle().compareTo(o.getTitle());
    }
}
