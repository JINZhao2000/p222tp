package cyou.zhaojin.pokemon;

import androidx.appcompat.app.AppCompatActivity;
import androidx.annotation.NonNull;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;

import java.lang.reflect.Method;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {
    private static String TAG = "MainActivity:LOG";
    private static final String API_BASE_URL = "https://pokeapi.co/api/v2/";
    private static final String API_IMAGE = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/";

    private ListView listView;
    private List<ListItem> liste = new ArrayList<>();
    private MainListViewAdapter adapter;
    private Resources res;
    private Drawable d;

    private int pngOffset = -50;
    private int count = 0;

    Retrofit retrofit;
    PokemenAPIService servicePokemon;

    @SuppressLint("UseCompatLoadingForDrawables")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        res = this.getResources();
        d = res.getDrawable(R.drawable.ic_pikachu, res.newTheme());

        listView = findViewById(R.id.liste);
        adapter = new MainListViewAdapter();

        this.retrofit = new Retrofit.Builder()
                .baseUrl(MainActivity.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        this.servicePokemon = retrofit.create(PokemenAPIService.class);

        Toast.makeText(MainActivity.this, R.string.bonjour, Toast.LENGTH_SHORT).show();

        Button button = (Button) findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonClic2();
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, MainActivity2.class);
                intent.putExtra("pokemon", liste.get(position).getPid());
                startActivity(intent);
            }
        });
    }

    public void buttonClic() {
        Toast.makeText(MainActivity.this, R.string.clic, Toast.LENGTH_SHORT).show();
        Log.d(MainActivity.TAG, "Clic");
        String uniqueId = UUID.randomUUID().toString();
        liste.add(new ListItem(uniqueId, d));
        listView.setAdapter(adapter);
    }

    public void buttonClic2() {
        pngOffset+=50;
        if(pngOffset>count&&count!=0){
            return;
        }
        Call<JsonElement> appel = null;
        if(pngOffset+50>count&&count!=0){
            appel = servicePokemon.listPokemons(API_BASE_URL+"/pokemon?limit="+(count-pngOffset)+"&offset="+pngOffset);
        }else{
            appel = servicePokemon.listPokemons(API_BASE_URL+"./pokemon?limit=50&offset="+pngOffset);
        }
        appel.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                if (response.isSuccessful()) {
                    JsonElement contenu = response.body();
                    // 1er niveau de traitement : on affiche le contenu "brut"
                    //textViewJSON.setText(contenu.toString());

                    JsonObject jsonGlobal = contenu.getAsJsonObject();
                    int count = jsonGlobal.get("count").getAsInt();

                    Toast.makeText(MainActivity.this, "Nb Pokemons total : " + count, Toast.LENGTH_SHORT).show();
                    if (MainActivity.this.count == 0) {
                        MainActivity.this.count = count;
                    }
                    JsonArray listePokemons = jsonGlobal.getAsJsonArray("results");

                    ImageView imageView = new ImageView(MainActivity.this);
                    ListItem item;
                    // StringBuffer chaineConstruite = new StringBuffer();
                    for (int i = 0; i < listePokemons.size(); i++) {
                        JsonObject unPokemon = listePokemons.get(i).getAsJsonObject();
                        item = new ListItem();
                        int pid = pngOffset + i + 1;
                        String url = API_IMAGE + pid + ".png";
                        item.setPid(pid);
                        item.setImage(new BitmapDrawable(res, new DownloadImageTask(imageView).doInBackground(url)));
                        item.setTitle(unPokemon.get("name").getAsString());
                        liste.add(item);
                        // chaineConstruite.append(unPokemon.get("name").getAsString()+"\n");
                    }
                    //Collections.sort(liste);
                    listView.setAdapter(adapter);
                } else {
                    Toast.makeText(MainActivity.this, "Erreur lors de l'appel à l'API :" + response.errorBody(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Erreur lors de l'appel à l'API :" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.ic_menu_add, menu);
        inflater.inflate(R.menu.ic_menu_quit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.ic_menu_add:
                buttonClic2();
                return true;
            case R.id.ic_menu_quit:
                System.exit(0);
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    class MainListViewAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return liste.size();
        }

        @Override
        public Object getItem(int position) {
            return liste.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ListItemView listItemView;
            if (convertView == null) {
                convertView = LayoutInflater.from(MainActivity.this).inflate(
                        R.layout.item, null);
                listItemView = new ListItemView();
                listItemView.imageView = (ImageView) convertView
                        .findViewById(R.id.image);
                listItemView.textView = (TextView) convertView
                        .findViewById(R.id.title);

                convertView.setTag(listItemView);
            } else {
                listItemView = (ListItemView) convertView.getTag();
            }
            Drawable img = liste.get(position).getImage();
            String title = liste.get(position).getTitle();
            listItemView.imageView.setImageDrawable(img);
            listItemView.textView.setText(title);
            return convertView;
        }
    }
}